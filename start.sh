#!/bin/bash


echo -e '#!/bin/bash\n\npip install jupyter-collaboration==2.0.11' > /tmp/install-jupyter-collaboration.sh &&
chmod +x /tmp/install-jupyter-collaboration.sh && 
docker pull jupyter/base-notebook:latest && 
docker compose down -v &&
docker compose up -d --build stable27 &&
docker compose logs -f stable27
