# Nextcloud development environment on Docker Compose

Nextcloud development environment using docker-compose


## First installation for NextPyter

Now, compile the application:
```bash
git submodule update --init
cd data/apps-extra/nextpyter
make
```
(ignore eventual vulnerabilities, you're in your local environment, unimportant)

Return to the base directory, and copy the example.env in `.env`:
```bash
export DOCKER_GROUP=$(getent group docker | cut -d: -f3)  && cat example.env | envsubst > .env
```


Now it get messed! You have to bootstrap the server

```bash
./bootstrap.sh

cd workspace/server
git fetch --unshallow
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin

git worktree add ../stable27 stable27
cd ../stable27
git submodule update --init
```

Go back to the base directory and run `start.sh` each time you need to run the dev server
```bash
./start.sh
```

### Futher configuration

Original project at: https://github.com/juliushaertl/nextcloud-docker-dev

For further information, like PHP debug, refer to the original repo.