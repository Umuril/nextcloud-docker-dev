#!/bin/bash

docker pull jupyter/docker-stacks-foundation:latest && 
docker pull jupyter/base-notebook:latest && 
docker pull jupyter/minimal-notebook:latest && 
docker pull jupyter/r-notebook:latest && 
docker pull jupyter/julia-notebook:latest && 
docker pull jupyter/scipy-notebook:latest && 
docker pull jupyter/tensorflow-notebook:latest && 
docker pull jupyter/datascience-notebook:latest && 
docker pull jupyter/pyspark-notebook:latest && 
docker pull jupyter/all-spark-notebook:latest